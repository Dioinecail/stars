﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/S_Stars"
{
	Properties
	{
		colorAmpBase("Sine Base", Float) = 0.5
		colorAmp("Sine Amp", Float) = 0.1
		colorFreq("Sine Freq", Float) = 0.5
		colorPower("Color Power", Float) = 1.0
		colorIntensity("Color Intensity", Float) = 1.0
		rotationSpeed("Rotation Speed", Float) = 1.0
		distanceMult("Rotation by distance multiplier", Float) = 1.0
		rotationScale("Rotation Scale Matrix", Vector) = (1.0, 1.0, 1.0, 1.0)
		rotationSinScale("Rotation Sin Scale Matrix", Vector) = (1.0, 1.0, 1.0, 1.0)
	}
	SubShader
	{
		Tags { "RenderType" = "Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct Star
			{
				float3 position;
				float scale;
				float4 color;
			};

			StructuredBuffer<Star> StarsBuffer;

			float colorAmp;
			float colorFreq;
			float colorAmpBase;
			float rotationSpeed;
			float colorIntensity;
			float colorPower;
			float distanceMult;
			float4 rotationScale;
			float4 rotationSinScale;

            struct v2f
            {
                float4 pos : SV_POSITION;
				float4 color : COLOR0;
            };

			static float3 Rotate(float3 input, float angle, int instanceID, float length)
			{
				float sine = sin(instanceID);
				float r = radians(angle * sine);
				float rLen = radians(angle * length);
				float3x3 rotationMatrix = float3x3 (
					cos(rLen * rotationScale.x) * rotationSinScale.x,
					0, 
					sin(rLen * rotationScale.y) * rotationSinScale.y,
					0, 
					1, 
					0, 
					-sin(rLen * rotationScale.z) * rotationSinScale.z,
					0, 
					cos(rLen * rotationScale.w) * rotationSinScale.w);
				return mul(rotationMatrix, input);
			}

            v2f vert (appdata_full v, uint instanceID : SV_InstanceID)
            {
				// Get rotation offset by instance ID
				float rotation = _Time.x * rotationSpeed + instanceID;
				float3 instancePosition = StarsBuffer[instanceID].position;

				// Rotate position around Y axis
				float distanceFromCenter = length(float2(instancePosition.x, instancePosition.z));
				instancePosition = Rotate(instancePosition, rotation, instanceID, distanceFromCenter * distanceMult);

				// Scale vertics by instance scale factor
				float instanceScale = StarsBuffer[instanceID].scale;
				float3 vertex = mul(v.vertex, instanceScale);
				float3 vertexWorldPosition = instancePosition + vertex;

				// Animate color by sine wave
				float colorSine = colorAmpBase + sin(instanceID + _Time.x * colorFreq) * colorAmp;
				float4 color = mul(StarsBuffer[instanceID].color, colorSine);

				// Apply stuff
                v2f o;
				o.pos = mul(UNITY_MATRIX_VP, float4(vertexWorldPosition, 1));
				o.color = pow(color, colorPower) * colorIntensity;
                return o;
            }

            float4 frag (v2f i) : SV_Target
            {
				float4 col = i.color;
                return col;
            }
            ENDCG
        }
    }
}
