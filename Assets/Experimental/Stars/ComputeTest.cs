﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;
using System.Linq;

public struct Star
{
    public Vector3 position;
    public float scale;
    public Color color;

    public Star(Vector3 _pos, float _scale, Color _color)
    {
        position = _pos;
        scale = _scale;
        color = _color;
    }
}

public class ComputeTest : MonoBehaviour
{
    public StarsSettings settings;

    ComputeBuffer starsBuffer;
    ComputeBuffer argsBuffer;



    public void Generate()
    {
        argsBuffer = new ComputeBuffer(1, 5 * sizeof(uint), ComputeBufferType.IndirectArguments);
        argsBuffer.SetData(new uint[] { settings.mesh.GetIndexCount(0), (uint)settings.numInstances, 0, 0, 0 });

        Star[] starsArray = new Star[settings.numInstances];

        for (int i = 0; i < starsArray.Length; i++)
        {
            float scale = GetRandomScale(settings.scaleMin, settings.scaleMax);
            starsArray[i] = new Star(GetRandomVector(settings.positionRadiusMin, settings.positionRadiusMax, settings.positionCurve), scale, GetRandomColor(GetRandomColorScale()));
        }

        starsBuffer = new ComputeBuffer(settings.numInstances, sizeof(float) * 3 + sizeof(float) + sizeof(float) * 4);
        starsBuffer.SetData(starsArray);
        settings.material.SetBuffer("StarsBuffer", starsBuffer);
    }

    private void Start()
    {
        Generate();
    }

    private void Update()
    {
        Graphics.DrawMeshInstancedIndirect(settings.mesh, 0, settings.material, GetBounds(settings.positionRadiusMax), argsBuffer);
    }

    private Vector3 GetRandomVector(float radiusMin, float radiusMax, AnimationCurve positionCurve)
    {
        float verticalDensity = 0;
        //float currentDensity = 0;
        Vector3 resultVector = Vector3.zero;

        int textureWidth = settings.densityMap.width;
        int textureHeight = settings.densityMap.height;

        for (int i = 0; i < settings.densityIterations; i++)
        {
            Vector3 v = Vector3.up;

            float x = Random.Range(0, 360);
            float y = Random.Range(0, 360);
            float z = Random.Range(0, 360);

            v = Quaternion.Euler(x, y, z) * v;

            Vector2 horizontal = new Vector2(v.x, v.z);
            float length = horizontal.magnitude;

            float verticalScale = positionCurve.Evaluate(length);

            //float sampleXPos = (v.x + v.y + v.z) / 3f;
            //float sampleYPos = (v.x * v.y * v.z) * 3;

            //float densitySampleX = settings.densityMap.GetPixel(Mathf.RoundToInt(sampleXPos * textureWidth), Mathf.RoundToInt(sampleYPos * textureHeight)).r;

            if (verticalDensity < verticalScale)
            {
                verticalDensity = verticalScale;
                resultVector = v;
            }
        }


        resultVector.y *= verticalDensity;

        float randomRadius = Random.Range(0f, 1f);
        return resultVector * Mathf.Lerp(radiusMin, radiusMax, randomRadius);
    }

    private float GetRandomScale(float min, float max)
    {
        float randomCurveValue = Random.Range(0f, 1f);
        float scale = Mathf.Lerp(min, max, settings.scaleCurve.Evaluate(randomCurveValue));
        return scale;
    }

    private float GetRandomColorScale()
    {
        float colorCurveValue = Random.Range(0f, 1f);
        return settings.colorScaleCurve.Evaluate(colorCurveValue);
    }

    private Color GetRandomColor(float scale)
    {
        return settings.GetColor(scale);
    }

    private Bounds GetBounds(float radius)
    {
        return new Bounds(Vector3.zero, Vector3.one * radius * 2);
    }

    private void OnValidate()
    {
        if (Application.isPlaying)
            Generate();
    }
}