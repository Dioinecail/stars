﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;
using System;

#if UNITY_EDITOR

[UnityEditor.CustomEditor(typeof(ComputeTest))]
public class ComputeTestEditor : UnityEditor.Editor
{
    ComputeTest test;

    SerializedProperty settingsReferense;
    SerializedProperty[] settingsProperties;



    private void OnEnable()
    {
        test = target as ComputeTest;
        settingsReferense = serializedObject.FindProperty("settings");
        settingsProperties = GetProperties(serializedObject);
    }

    public override void OnInspectorGUI()
    {
        EditorGUILayout.LabelField("Settings Referense", EditorStyles.boldLabel);
        EditorGUILayout.ObjectField(settingsReferense);


        EditorGUILayout.Space(15);
        EditorGUILayout.LabelField("Settings Editor", EditorStyles.boldLabel);

        if (settingsProperties != null && settingsProperties.Length > 0)
            DisplaySerializedProperties(settingsProperties);

        if (GUILayout.Button("Generate"))
        {
            test.Generate();
        }
    }

    private void DisplaySerializedProperties(SerializedProperty[] settingsProperties)
    {
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.BeginVertical();

        SerializedObject settingsSerialized = settingsProperties[0].serializedObject;

        foreach (var p in settingsProperties)
        {
            EditorGUILayout.PropertyField(p);
        }

        EditorGUILayout.EndVertical();
        if (EditorGUI.EndChangeCheck())
        {
            serializedObject.ApplyModifiedProperties();
            settingsSerialized.ApplyModifiedProperties();
            test.Generate();
        }
    }

    private SerializedProperty[] GetProperties(SerializedObject target)
    {
        List<SerializedProperty> props = new List<SerializedProperty>();

        var fields = typeof(StarsSettings).GetFields(BindingFlags.Instance | BindingFlags.Public);

        SerializedProperty settings = serializedObject.FindProperty("settings");

        SerializedObject serializedSettings = new SerializedObject(settings.objectReferenceValue);

        foreach (var f in fields)
        {
            SerializedProperty p = serializedSettings.FindProperty(f.Name);
            props.Add(p);
        }

        return props.ToArray();
    }
}

#endif