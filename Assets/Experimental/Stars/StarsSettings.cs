﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class StarsSettings : ScriptableObject
{
    public float positionRadiusMin = 0.5f;
    public float positionRadiusMax = 4;
    public AnimationCurve positionCurve;
    public float scaleMin = 1, scaleMax = 4;
    public Gradient colorGradient;
    public AnimationCurve scaleCurve;
    public AnimationCurve colorScaleCurve;
    public int densityIterations = 3;
    public int numInstances = 50000;
    public Texture2D densityMap;
    public Material material;
    public Mesh mesh;

    public bool debugColors;



    public Color GetColor(float scale)
    {
        if (debugColors)
            return Color.white;
        else
            return colorGradient.Evaluate(scale);
    }
}